﻿using EFDummy.Data.Models;
using Microsoft.EntityFrameworkCore;
using System;

namespace EFDummy.Data
{
    public class EFDummyDbContext : DbContext
    {
        public EFDummyDbContext(DbContextOptions<EFDummyDbContext> options) : base(options)
        {
        }

        public DbSet<Person> Persons { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Person>().ToTable("Person");
        }
    }
}
