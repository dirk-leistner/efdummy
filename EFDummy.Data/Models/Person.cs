﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EFDummy.Data.Models
{
    public class Person
    {
        public Int64 id { get; set; }
        public string firstname { get; set; }
        public string lastname { get; set; }
    }
}
